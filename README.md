# This is tests.opengjk

A project containing unit tests for [openGJK library](https://github.com/MattiaMontanari/openGJK). On its own it does nothing, you need to clone it with the openGJK library to run tests and add your own.

Why having the unit tests separate from the library repo? Just to keep the openGJK repository as clean and light as possible. Many users are not C/C++ developers and I want to provided them with nothing but a portable library that is simple to use. For developers I have repos like this one.

## Usage

### Prerequisites

A standard C/C++ toolchain is required along with CMake and [CMocka](https://cmocka.org/). The simplest way to install CMocka is to download it and build from source - I wrote something about [using CMocka here](https://www.mattiamontanari.com/blogs/cmocka/).

The rest is all handled by CMake and fits a rigid structure. This project needs to find in its parent folder the openGJK library and its `CMakeLists.txt`.

## Support
If you need help, any help, just reach out or create a new issue.

## License
[GPL-3.0 license](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Project status
The project is fairly mature and now I maintain it, along with the openGJK library, in my spare time. I am available to work with customers in a consultancy capacity to develop features and port on specific platforms.
